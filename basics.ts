// Primitives: number, string, boolean
// Complex types: arrays, objects
// Function types, parameters

// Primitives
let age: number = 13;
let userName: string = "Hungnx1330";
let isInstructor: boolean;

// Complex types
let hobbies: string[];
hobbies = ['Sports', 'Cooking', 'Music'];

let person: {
    name: string;
    age: number;
};

person = {
    name: 'Max',
    age: 25
};

let course: string | number | boolean;
course = "abc";
course = 123;
// course = [123, 123];
course = false;

// Functions & Types
class Student{
    // firstName: string;
    // lastName: string;
    // age: number;
    // courses: string[];

    constructor(
        public firstName: string, 
        public lastName: string, 
        public age: number, 
        private courses: string[]) {}

    enroll(courseName: string){
        this.courses.push(courseName);
    }
}

const student = new Student('Max', 'Schwarz', 32, ['Angular']);
student.enroll('React');